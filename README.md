# Big_G_Search

Script python pour de la recherche d'information(s) à travers différents arguments.

**Sommaire**
 - 1. Utilisation
 - 2. Installation

## 1. Utilisation
    USAGE
        bigGsearch.py [options obligatoire...] [options facultatives...]


    OBLIGATOIRE
    Recherche d'un mot unique dans un fichier spécifique:
        Court: bigGsearch.py -f C:\exemple.txt -w admin
        Long: bigGsearch.py --file C:\exemple.txt --word admin

    Recherche depuis un dictionnaire dans un fichier spécifique:
        Court: bigGsearch.py -f C:\exemple.txt -D C:\dict.txt
        Long: bigGsearch.py --file C:\exemple.txt --dict C:\dict.txt\

    Recherche d'un mot unique dans un ou plusieurs dossier(s):
        Court: bigGsearch.py -d C:\dossier\ -w admin
        Long: bigGsearch.py --dossier C:\dossier\ --word admin

    Recherche depuis un dictionnaire dans un ou plusieurs dossier(s):
        Court: bigGsearch.py -d C:\dossier\ -D C:\dict.txt
        Long: bigGsearch.py --dossier C:\dossier\ --dict C:\dict.txt\


    FACULTATIF
    Recherche résursive pour les recherches dans les dossiers:
        Court: bigGsearch.py [options obligatoire...] -r
        Long: bigGsearch.py [options obligatoire...] --recursive

 ## 2. Installation
### Installation avec build *(pour éviter le téléchargement des modules)*
 1. Téléchargez le fichier: https://gitlab.com/hasyame/big_g_search/-/blob/main/dist/Big%20G%20Search-0.1.0.tar.gz
 2. Dézippez-le.
 3. Exécutez setup.py build
 4. Dans build\scripts-3.10, exécutez bigGsearch.py
 5. Have fun :)

### Installation simple
 1. Téléchargez le fichier: https://gitlab.com/hasyame/big_g_search/-/blob/main/dist/Big%20G%20Search-0.1.0.tar.gz
 2. Dézippez-le.
 3. (OPTIONNEL) Installez les modules requis avec pip install -r requirements.txt
 4. Exécutez bigGsearch.py à la racine.
