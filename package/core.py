#!/usr/bin/env python3
# Script principal permettant d'exec un grep d'info sur un ensemble de fichiers

# Librairies
import sys
import getopt

# Fonctions importées
from .fc_dossierWithDict import dWD
from .fc_fileWithDict import fWD
from .fc_fileWithWord import fWW
from .fc_dossierWithWord import dWW
from .fc_help import helpLong
from .fc_help import rapidhelp
from .fc_usage import gitdl


# Fonction d'exec du script
def main():
    # Variables
    recursive_search = 0
    file_to_search = ""
    word_to_search = ""
    dict_search = ""
    dossier_search = ""
    output_file = ""
    dlgitrepo = ""

    # Liste d'arguments
    argumentList = sys.argv[1:]

    # Options courtes
    options = "hd:rf:D:w:o:t:F:a"

    # Options longues
    long_options = ["help", "dossier=", "recursive", "file=", "dict=", "word=",
                    "output", "thread=", "fuzzdb", "auto"]

    # Exec des arugments
    try:
        arguments, values = getopt.getopt(argumentList, options, long_options)
        for currentArgument, currentValue in arguments:
            if currentArgument in ("-h", "--help"):
                helpLong()
                sys.exit()
            elif currentArgument in ("-d", "--dossier"):
                dossier_search = currentValue
            elif currentArgument in ("-r", "--recursive"):
                recursive_search = 1
            elif currentArgument in ("-f", "--file"):
                file_to_search = currentValue
            elif currentArgument in ("-D", "--dict"):
                dict_search = currentValue
            elif currentArgument in ("-w", "--word"):
                word_to_search = currentValue
            elif currentArgument in ("-o", "--output"):
                output_file = currentValue
            elif currentArgument in ("-t", "--thread"):
                thread = currentValue
                print(thread)
            elif currentArgument in ("-F", "--fuzzdb"):
                thread = currentValue
            elif currentArgument in ("-a", "--auto"):
                dlgitrepo = True
                print("Option en cours de développement")
            else:
                sys.exit()

        if len(arguments) == 0:
            print("Vous n'avez pas renseigné d'arguments.")
            print("Veuillez-vous renseigner avec -h ou --help.")
            print("Exemple: ./bigGsearch.py -h")

        # Bloc de recherche par dossier(s)
        elif len(dossier_search) >= 1:
            if len(file_to_search) == 0:

                # Recerche récursif
                if recursive_search == 1:
                    if len(word_to_search) >= 1 and len(dict_search) >= 1:
                        print("Erreur: Le script ne peut pas prendre ces deux"
                              + " arguments en même temps. (l65)")
                        rapidhelp()
                    elif len(word_to_search) >= 1:
                        if dlgitrepo is True:
                            gitdl(dossier_search)
                        dWW(dossier_search, recursive_search, word_to_search,
                            output_file)
                    elif len(dict_search) >= 1:
                        if dlgitrepo is True:
                            gitdl(dossier_search)
                        dWD(dossier_search, recursive_search, dict_search,
                            output_file)
                    else:
                        print("Il manque le mot ou le dictionnaire.")
                        rapidhelp()

                # Recerche sans récursivité
                elif recursive_search == 0:
                    if len(word_to_search) >= 1 and len(dict_search) >= 1:
                        print("Erreur: Le script ne peut pas prendre ces deux"
                              + " arguments en même temps. (l79)")
                        rapidhelp()
                    elif len(word_to_search) >= 1:
                        dWW(dossier_search, recursive_search, word_to_search,
                            output_file)
                    elif len(dict_search) >= 1:
                        dWD(dossier_search, recursive_search, dict_search,
                            output_file)
                    else:
                        print("Il manque le mot ou le dictionnaire.")
                        rapidhelp()
            elif len(file_to_search) != 0:
                print("Erreur: Le script ne peut pas prendre ces deux"
                      + " arguments en même temps. (l92)")
                rapidhelp()
            else:
                pass

        # Bloc de recherche par fichier unique
        elif len(file_to_search) >= 1:
            if len(dossier_search) == 0:
                if len(word_to_search) >= 1 and len(dict_search) >= 1:
                    print("ligne 102")
                    print("Erreur: Le script ne peut pas prendre ces deux"
                          + " arguments en même temps. (l103)")
                    rapidhelp()
                elif len(word_to_search) >= 1:
                    fWW(file_to_search, word_to_search, output_file)
                elif len(dict_search) >= 1:
                    fWD(file_to_search, dict_search, output_file)
            elif len(dossier_search) != 0:
                print("Erreur: Le script ne peut pas prendre ces deux"
                      + " arguments en même temps. (l111)")
                rapidhelp()
            else:
                print("Une erreur inconnue est apparue. (l114)")
                rapidhelp()

        # Bloc de fin
        else:
            print("Une erreur inconnue est apparue. (l135)")
            rapidhelp()

    except getopt.error as err:
        print(str(err))


# Exec de la fonction principale
if __name__ == '__main__':
    main()
