#!/usr/bin/env python3

# import interne
from .fc_usage import csvGen
from .fc_usage import createResult
from .fc_usage import nameResult
from .fc_usage import banner
from .fc_usage import checkFile

# import ext
import os
import shlex
import re
import sys

# Variables
list_dict_word = []
list_file_word = []
list_to_save = []
file_in_folder = []
filenames = None
count = None


# Fonction
def dWD(dossier_search, recursive_search, dict_search, output_file):
    # Variables
    hit = 0
    line_total = 0
    totalligne = 0
    username = os.getlogin()
    full_Log_Path = "C:\\Users\\" + username + "\\Documents\\bigGsearchLog\\"

    # Création du fichier de log
    output_file_name = nameResult(output_file)
    createResult(output_file_name)

    banner()
    # Début du traitement de la boucle
    # Avec récursivité
    if recursive_search == 1:
        num_files = sum([len(files) for r, d, files in
                         os.walk(dossier_search)])
        num_actual_file = 1
        for root, directories, filenames in \
                os.walk(dossier_search):
            for filename in filenames:
                num_file_pourcentage = (num_actual_file / num_files) * 100
                line_number = 1
                file_to_search = os.path.abspath(root + '\\' + filename)
                if checkFile(file_to_search) == 1:
                    print("Le fichier " + file_to_search + " est illisible.")
                elif checkFile(file_to_search) == 0:
                    with open(file_to_search, encoding="utf8") as file:
                        for count, line in enumerate(file):
                            pass
                    print("------------------------------")
                    print("Analyse de: " + file_to_search)
                    print("Fichier: " + str(num_actual_file) +
                          "/" + str(num_files))
                    print("Pourcentage: " + str(num_file_pourcentage) + "%")
                    print("Nombre de lignes: " + str(count + 1))
                    print("Log présent dans: " + str(full_Log_Path))
                    totalligne = (count + 1) + totalligne
                    print("Total de lignes analysées: " + str(totalligne))
                    print("------------------------------")
                    with open(dict_search, encoding="utf8") as fileDict:
                        for word_to_search in fileDict:
                            wordtosearch_lower = word_to_search.lower().rstrip()
                            with open(file_to_search, encoding="utf8") as file:
                                str_search = file.read().splitlines()
                                for line in str_search:
                                    line_lower = line.lower()
                                    if len(str(line_lower)) <= 256:
                                        if re.search(wordtosearch_lower, line_lower):
                                            if len(output_file) >= 1:
                                                print(file_to_search + " ligne "
                                                      + str(line_number) + " | "
                                                      + line.rstrip() + " -> "
                                                      + word_to_search.rstrip())
                                                csvGen(file_to_search, line_number,
                                                       line.rstrip(),
                                                       word_to_search.rstrip(),
                                                       output_file_name)
                                                hit += 1
                                            elif len(output_file) == 0:
                                                print(file_to_search + " | ligne "
                                                      + str(line_number)
                                                      + " | " + line.rstrip()
                                                      + " -> "
                                                      + word_to_search.rstrip())
                                                hit += 1
                                            else:
                                                print("Erreur lors de la génératio"
                                                      + "n du fichier de résultat.")
                                                sys.exit()
                                        else:
                                            continue
                                    if len(str(line_lower)) > 256:
                                        print("La valeur du mot recherché n'est pas exploitable")

                                    else:
                                        print("Une erreur est apparu lors du traitement de la ligne " + str(line_number))

                                    line_number = line_number + 1
                                    line_total = line_total + 1

                    clear_command = "cls"
                    os.system(shlex.quote(clear_command))
                else:
                    print("Une erreur est arrivé.")
                print("\n")
                num_actual_file = num_actual_file + 1
        print("Nombre de match: " + str(hit))
        print("Total de lignes analysées: " + str(totalligne))

    # Sans récursivité
    elif recursive_search == 0:
        num_actual_file = 1
        num_files = 0
        for path in os.listdir(dossier_search):
            if os.path.isfile(os.path.join(dossier_search, path)):
                num_files += 1
        for (dirpath, dirnames, filenames) in os.walk(dossier_search):
            file_in_folder.extend(filenames)
            break
        for filename in filenames:
            num_file_pourcentage = (num_actual_file / num_files) * 100
            line_number = 1
            file_to_search = dossier_search + '\\' + filename
            try:
                with open(file_to_search, encoding="utf8") as file:
                    for count, line in enumerate(file):
                        pass
            except ValueError:
                print("Erreur de décode sur le fichier, ça continue !!")
                continue
            print("------------------------------")
            print("Analyse de: " + file_to_search)
            print("Fichier: " + str(num_actual_file) + "/"
                  + str(num_files))
            print("Nombre de lignes: " + str(count + 1))
            print("Pourcentage: " + str(num_file_pourcentage) + "%")
            totalligne = (count + 1) + totalligne
            print("Total de lignes analysées: " + str(totalligne))
            print("------------------------------")
            with open(dict_search, encoding="utf8") as fileDict:
                for word_to_search in fileDict:
                    wordtosearch_lower = word_to_search.lower().rstrip()
                    with open(file_to_search, encoding="utf8") as file:
                        str_search = file.read().splitlines()
                        for line in str_search:
                            line_lower = line.lower()
                            if len(str(line_lower)) <= 256:
                                if re.search(wordtosearch_lower, line_lower):
                                    if len(output_file) >= 1:
                                        print(file_to_search + " ligne "
                                              + str(line_number) + " | "
                                              + line.rstrip() + " -> "
                                              + word_to_search.rstrip() + " | "
                                              + str(len(line)))
                                        csvGen(file_to_search, line_number,
                                               line.rstrip(),
                                               word_to_search.rstrip(),
                                               output_file_name)
                                        hit += 1
                                    elif len(output_file) == 0:
                                        print(file_to_search + " | ligne "
                                              + str(line_number)
                                              + " | " + line.rstrip() + " -> "
                                              + word_to_search.rstrip() + " | "
                                              + str(len(line_lower)))
                                        hit += 1
                                    else:
                                        print("Erreur lors de la génération du"
                                              + " fichier de résultat.")
                                        sys.exit()
                                else:
                                    continue

                            if len(str(line_lower)) > 256:
                                print("La valeur du mot recherché n'est pas exploitable")

                            else:
                                print("Une erreur est apparu lors du traitement de la ligne " + str(line_number))

                            line_number = line_number + 1
                            line_total = line_total + 1
            print("\n")
            num_actual_file = num_actual_file + 1
        print("Nombre de match: " + str(hit))
        print("Nombre de lignes analysées: " + str(line_total))

    else:
        print("Il y a un problème (l41)")
