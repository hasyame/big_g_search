#!/usr/bin/env python3
# import ext
import sys

# Variables
list_dict_word = []
list_file_word = []
list_to_save = []


# Fonctions
def helpLong():
    print("Usage: bigGsearch.py [options obligatoire...]"
          + " [options facultatives...]\n")

    print("\t ~OBLIGATOIRE~")
    print("\t Recherche d'un mot unique dans un fichier spécifique:")
    print("\t \t Court: bigGsearch.py -f C:\\exemple.txt -w admin")
    print("\t \t Long: bigGsearch.py --file C:\\exemple.txt --word admin\n")

    print("\t Recherche depuis un dictionnaire dans un fichier spécifique:")
    print("\t \t Court: bigGsearch.py -f C:\\exemple.txt -D C:\\dict.txt")
    print("\t \t Long: bigGsearch.py --file C:\\exemple.txt --dict"
          + " C:\\dict.txt\n")

    print("\t Recherche d'un mot unique dans un dossier:")
    print("\t \t Court: bigGsearch.py -d C:\\dossier\\ -w admin")
    print("\t \t Long: bigGsearch.py --dossier C:\\dossier\\ --word admin\n")

    print("\t Recherche depuis un dictionnaire dans un dossier:")
    print("\t \t Court: bigGsearch.py -d C:\\dossier\\ -D C:\\dict.txt")
    print("\t \t Long: bigGsearch.py --dossier C:\\dossier\\ --dict"
          + " C:\\dict.txt\n")

    print("\t ~FACULTATIF~")
    print("\t Recherche résursive pour les recherches dans les dossiers:")
    print("\t \t Court: bigGsearch.py [options obligatoire...] -r")
    print("\t \t Long: bigGsearch.py [options obligatoire...] --recursive")

    print("Fichier de sortie (csv), conserver des Mes Documents/BigSearchLog:")
    print("\t \t Court: bigGsearch.py [options obligatoire...] -o NOM")
    print("\t \t Long: bigGsearch.py [options obligatoire...] --output NOM")


def rapidhelp():
    print("Veuillez-vous renseigner avec -h ou --help.")
    print("Exemple: ./bigGsearch.py -h")
    sys.exit()
