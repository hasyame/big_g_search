#!/usr/bin/env python3
import csv
import time
import sys
import os
import stashy
import urllib3
from git import Repo
import getpass

# Désactivation du message d'alerte certificat autosigné
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Variables
count = None


# Fonctions appelées
def csvGen(file, line_number, search_word, word, output_file_name):
    if len(output_file_name) >= 1:
        list_to_save = []
        list_to_save.append(file)
        list_to_save.append(line_number)
        list_to_save.append(search_word)
        list_to_save.append(word)
        username = os.getlogin()
        log_Path = "C:\\Users\\" + username + "\\Documents\\bigGsearchLog\\"
        full_Log_Path = str(log_Path) + str(output_file_name)

        # Création du dossier de log
        if not os.path.exists(log_Path):
            os.makedirs(log_Path)

        # Création du fichier CSV
        with open(full_Log_Path, 'a', newline='', encoding="utf8") as fichiercsv:
            writer = csv.writer(fichiercsv, delimiter='¤')
            writer.writerow(list_to_save)
    else:
        pass


def createResult(output_file):
    if len(output_file) >= 1:
        # Variables
        username = os.getlogin()
        log_Path = "C:\\Users\\" + username + "\\Documents\\bigGsearchLog\\"
        full_Log_Path = str(log_Path) + str(output_file)

        # Création du dossier de log
        if not os.path.exists(log_Path):
            os.makedirs(log_Path)

        with open(full_Log_Path, 'a', newline='', encoding="utf8") as fichiercsv:
            writer = csv.writer(fichiercsv, delimiter="¤")
            writer.writerow(['Fichier', 'Numéro de ligne', 'Match',
                             'Mot recherché'])
    elif len(output_file) == 0:
        pass
    else:
        print("Il y a un problème avec le fichier de résultat.")
        sys.exit()


def nameResult(output_file):
    if len(output_file) >= 1:
        timestr = time.strftime("%Y%m%d-%H%M%S")
        output_file_name = timestr + "-" + output_file + ".csv"
    elif len(output_file) == 0:
        output_file_name = ""
    else:
        print("Il y a un problème avec le fichier de résultat.")
        sys.exit()

    return output_file_name


def foundResult(output_file_name):
    if len(output_file_name) >= 1:
        with open(output_file_name, 'r', encoding="utf8") as fp:
            for count, line in enumerate(fp):
                pass
        print("")
        print("------------------------------")
        print("NE PAS FERMER LA FENETRE!!!!")
        print("------------------------------")
        print("Résumé")
        print('Nombre de résultats: ', count)
        print("Veuillez utilisez:  ¤  comme séparateur pour le CSV")
    elif len(output_file_name) == 0:
        pass
    else:
        print("Il y a un problème avec le fichier de résultat.")
        sys.exit()


def banner():
    print(" ____  ____  ___     ___    ___  ____    __    ____   ___  _   _")
    print("(  _ \\(_  _)/ __)   / __)  / __)( ___)  /__\\  (  _ \\ / __)( )_"
          + "( )")
    print(" ) _ < _)(_( (_-.  ( (_-.  \\__ \\ )__)  /(__)\\  )   /( (__  ) _ "
          + "(")
    print("(____/(____)\\___/   \\___/  (___/(____)(__)(__)(_)\\_) \\___)(_) "
          + "(_)\n")
    print("Par Hasyame")


def checkFile(file_to_search):
    try:
        with open(file_to_search, encoding="utf8") as file:
            for count, line in enumerate(file):
                pass
        return 0
    except UnicodeDecodeError:
        return 1


def gitdl(dossierStockage):
    boucle = True
    nbr_list = 0
    username = input("Nom d'utilisateur: ")
    password = getpass.getpass('Mot de passe: ')
    if password == "":
        print("Vous n'avez pas mis de mot de passe.")
    host = input("URL de login: ")
    host = host.replace("https://","")
    host = host.replace("http://","")
    host = host.replace("/","")
    project = input("Nom du projet: ")
    SSL = input("Certificat auto-signé: [O/N] ")

    if SSL == "O":
        SSLverify = False
        print("Il est recommandé d'ajouter ou de ne pas utiliser de certificat autosigné.")
    elif SSL == "N":
        SSLverify = True
    else:
        print("Vous n'avez pas répondu correctement.")
        SSLverify = True

    bitbucket = stashy.connect(
        "http://" + host + "/",
        username,
        password,
        verify=SSLverify)

    while boucle is not False:
        try:
            repos = bitbucket.projects[project].repos.list()
            json_value = repos[nbr_list]
            json_value_name = json_value['slug']
            print(json_value_name)
            full_local_path = dossierStockage
            remote = f"https://{username}@{host}/scm/{project}/{json_value_name}.git"
            Repo.clone_from(
                remote,
                full_local_path + "\\" + json_value_name,
                config='http.sslVerify=false')
            nbr_list = nbr_list + 1
        except IndexError:
            boucle = False
