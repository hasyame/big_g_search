#!/usr/bin/env python3

# import interne
from .fc_usage import csvGen
from .fc_usage import createResult
from .fc_usage import nameResult
from .fc_usage import banner

# import ext
import os
import re
import sys

# Variables
list_dict_word = []
list_file_word = []
list_to_save = []
file_in_folder = []
filenames = None


# Fonction
def dWW(dossier_search, recursive_search, word_to_search, output_file):
    banner()
    hit = 0
    output_file_name = nameResult(output_file)
    createResult(output_file_name)
    # Si -r (recursive)
    if recursive_search == 1:
        for root, directories, filenames in \
                os.walk(dossier_search):
            for filename in filenames:
                line_number = 0
                file_to_search = os.path.abspath(root + '\\' + filename)
                print("------------------------------")
                print("Analyse de: " + file_to_search)
                print("------------------------------")
                with open(file_to_search, encoding="utf8") as file:
                    for line in file:
                        line_number = line_number + 1
                        line_lower = line.lower()
                        wordtosearch_lower = word_to_search.lower()
                        if len(str(line_lower)) <= 256:
                            if re.search(wordtosearch_lower, line_lower):
                                if len(output_file) >= 1:
                                    print(file_to_search + " ligne "
                                          + str(line_number) + " | "
                                          + line.rstrip() + " -> "
                                          + word_to_search)
                                    csvGen(file_to_search, line_number,
                                           line.rstrip(), word_to_search,
                                           output_file_name)
                                elif len(output_file) == 0:
                                    print(file_to_search + " | ligne "
                                          + str(line_number)
                                          + " | " + line.rstrip() + " -> "
                                          + word_to_search)
                                    print("")
                                    print("Aucun fichier(s) de sortie.")
                                else:
                                    print("Erreur lors de la génération du fichier"
                                          + "  de résultat.")
                                    sys.exit()
                                hit += 1
                        if len(str(line_lower)) > 256:
                            print("La valeur du mot recherché n'est pas exploitable")

                        else:
                            print("Une erreur est apparu lors du traitement de la ligne " + str(line_number))

        print("Nombre de match: " + str(hit))

    # Sans -r (recursive)
    elif recursive_search == 0:
        for (dirpath, dirnames, filenames) in os.walk(dossier_search):
            file_in_folder.extend(filenames)
            break
        for filename in filenames:
            line_number = 0
            file_to_search = dossier_search + '\\' + filename
            print("------------------------------")
            print("Analyse de: " + file_to_search)
            print("------------------------------")
            with open(file_to_search, encoding="utf8") as file:
                for line in file:
                    line_number = line_number + 1
                    line_lower = line.lower()
                    wordtosearch_lower = word_to_search.lower()
                    if len(str(line_lower)) <= 256:
                        if re.search(wordtosearch_lower, line_lower):
                            if len(output_file) >= 1:
                                print(file_to_search + " ligne "
                                      + str(line_number) + " | "
                                      + line.rstrip() + " -> "
                                      + word_to_search)
                                csvGen(file_to_search, line_number,
                                       line.rstrip(), word_to_search,
                                       output_file_name)
                            elif len(output_file) == 0:
                                print(file_to_search + " | ligne "
                                      + str(line_number)
                                      + " | " + line.rstrip() + " -> "
                                      + word_to_search)
                                print("")
                                print("Aucun fichier(s) de sortie.")
                            else:
                                print("Erreur lors de la génération du fichier"
                                      + "  de résultat.")
                                sys.exit()
                            hit += 1
                    if len(str(line_lower)) > 256:
                        print("La valeur du mot recherché n'est pas exploitable")

                    else:
                        print("Une erreur est apparu lors du traitement de la ligne " + str(line_number))
        print("Nombre de match: " + str(hit))

    else:
        print("Il y a un problème (l41)")
