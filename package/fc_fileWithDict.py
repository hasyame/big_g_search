# import interne
from .fc_usage import csvGen
from .fc_usage import createResult
from .fc_usage import nameResult
from .fc_usage import foundResult
from .fc_usage import banner

# import ext
import re
import sys

# Variables
list_dict_word = []
list_file_word = []
list_to_save = []


# Fonction
def fWD(file_to_search, dict_search, output_file):
    banner()

    # Variables
    boucleDict = 0
    boucleSearch = 0
    hit = 0
    line_total = 0

    # Nom du fichier de sortie
    output_file_name = nameResult(output_file)
    createResult(output_file_name)

    with open(dict_search, encoding="utf8") as fileDict:
        for dict_line in fileDict:
            list_dict_word.append(dict_line.rstrip())
        len_dict = len(list_dict_word)

    with open(file_to_search, encoding="utf8") as fileSearch:
        for line in fileSearch:
            list_file_word.append(line.rstrip())
        len_search = len(list_file_word)
    print("------------------------------")
    print("Analyse de: " + file_to_search)
    print("------------------------------")
    while len_dict != boucleDict:
        boucleSearch = 0
        line_number = 1
        dict_word = list_dict_word[boucleDict]
        dict_word_lower = dict_word.lower()
        while len_search != boucleSearch:
            search_word = list_file_word[boucleSearch]
            search_word_lower = search_word.lower()
            if len(str(search_word)) <= 256:
                if re.search(dict_word_lower, search_word_lower):
                    if len(output_file) >= 1:
                        print(file_to_search + " | ligne " + str(line_number)
                              + " | " + search_word + " -> " + dict_word)
                        csvGen(file_to_search, line_number, search_word, dict_word,
                               output_file_name)
                        hit += 1
                    elif len(output_file) == 0:
                        print(file_to_search + " | ligne " + str(line_number)
                              + " | " + search_word + " -> " + dict_word)
                        print(len(search_word))
                        hit += 1
                    else:
                        print("Erreur lors de la génération du fichier de"
                              + " résultat.")
                        sys.exit()
            if len(str(search_word)) > 256:
                print("La valeur du mot recherché n'est pas exploitable")
            else:
                print("Une erreur est apparu lors du traitement de la ligne " + str(line_number))

            line_number = line_number + 1
            line_total = line_total + 1
            boucleSearch = boucleSearch + 1
        boucleDict = boucleDict + 1
    print("Nombre de match: " + str(hit))

    foundResult(output_file_name)
