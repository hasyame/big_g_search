#!/usr/bin/env python3

# import interne
from .fc_usage import csvGen
from .fc_usage import createResult
from .fc_usage import nameResult
from .fc_usage import banner

# import ext
import re
import sys

# Variables
list_dict_word = []
list_file_word = []
list_to_save = []


# Fonction
def fWW(file_to_search, word_to_search, output_file):
    # Variables
    hit = 0
    line_number = 0

    banner()

    output_file_name = nameResult(output_file)
    createResult(output_file_name)
    with open(file_to_search, encoding="utf8") as file:
        print("------------------------------")
        print("Analyse de: " + file_to_search)
        print("------------------------------")
        for line in file:
            line_number = line_number + 1
            line_lower = line.lower()
            wordtosearch_lower = word_to_search.lower()
            if re.search(wordtosearch_lower, line_lower):
                if len(line.rstrip()) <= 256:
                    if len(output_file) >= 1:
                        print(file_to_search + " ligne " + str(line_number) + " | "
                              + line.rstrip() + " -> " + word_to_search)
                        csvGen(file_to_search, line_number, line.rstrip(),
                               word_to_search, output_file_name)
                        hit += 1
                    elif len(output_file) == 0:
                        print(file_to_search + " | ligne " + str(line_number)
                              + " | " + line.rstrip() + " -> " + word_to_search)
                        hit += 1
                    else:
                        print("Erreur lors de la génération du fichier de"
                              + " résultat.")
                        sys.exit()
                else:
                    print("La valeur du mot recherché n'est pas exploitable")
        print("Nombre de match: " + str(hit))
