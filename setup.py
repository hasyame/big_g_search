#!/usr/bin/env python3
from setuptools import setup

with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license_read = f.read()

setup(
    name='Big G Search',
    version='0.1.0',
    description="Script python pour de la recherche d'information(s) à"
    + " travers différents arguments.",
    long_description=readme,
    author='Hasyame',
    author_email='Hasyame@protonmail.com',
    url='https://gitlab.com/hasyame/big_g_search',
    license=license_read,
    packages=['package'],
    scripts=['bigGsearch.py'],
    install_requires=["charset-normalizer"]
)
